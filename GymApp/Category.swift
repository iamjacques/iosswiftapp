//
//  Category.swift
//  GymApp
//
//  Created by Jacques Uwamun on 12/07/2019.
//  Copyright © 2019 Jacques Uwamun. All rights reserved.
//

import SwiftUI

struct Category: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var imageName: String
}
