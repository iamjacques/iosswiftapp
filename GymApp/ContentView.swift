//
//  ContentView.swift
//  GymApp
//
//  Created by Jacques Uwamun on 12/07/2019.
//  Copyright © 2019 Jacques Uwamun. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    let categoryName: String
    let categories: [Category]
    
    var body: some View {
        NavigationView{
            ScrollView{
                HStack {
                    ForEach(self.categories.identified(by: \.name)) { category in
                        CategoryItem(category: category )
                            .frame(width: 250.0, height: 200)
                        
                    }
                }
            }
            .navigationBarTitle(Text("Category"))
        }
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView(categoryName: "Category", categories: categoriesData)
    }
}
#endif
