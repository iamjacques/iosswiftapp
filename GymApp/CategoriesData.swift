//
//  CategoriesData.swift
//  GymApp
//
//  Created by Jacques Uwamun on 12/07/2019.
//  Copyright © 2019 Jacques Uwamun. All rights reserved.
//

import Foundation

let categoriesData:[Category] = loadData("Categories")

func loadData<T:Decodable>(_ filename:String, as type: T.Type = T.self) -> T {
    let data:Data
    // Get url for file
    guard let file = Bundle.main.url(forResource: filename, withExtension: "json") else {
        fatalError("File could not be located at the given url")
    }
    
    do {
        data = try Data(contentsOf: file)
        
    } catch {
        fatalError("Could not load the file data: \(error)")
    }
    
    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
        
    } catch {
        fatalError("Could not load the file data: \(error)")
    }
}
