//
//  CategoriesRow.swift
//  GymApp
//
//  Created by Jacques Uwamun on 12/07/2019.
//  Copyright © 2019 Jacques Uwamun. All rights reserved.
//

import SwiftUI

struct CategoriesRow : View {
    let categoryName: String
    let categories: [Category]
    
    var body: some View {
        
        VStack(alignment: .leading){
            
            Text("Gym App")
                .font(.headline)
                .color(.primary)
            
            ScrollView{
                HStack {
                    ForEach(self.categories.identified(by: \.name)) { category in
                        CategoryItem(category: category )
                            .frame(width: 250.0, height: 200)
                        
                    }
                }
            }
        }
    }

}

#if DEBUG
struct CategoriesRow_Previews : PreviewProvider {
    static var previews: some View {
        CategoriesRow(categoryName: "Category", categories: categoriesData)
    }
}
#endif
