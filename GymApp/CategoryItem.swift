//
//  CategoryItem.swift
//  GymApp
//
//  Created by Jacques Uwamun on 12/07/2019.
//  Copyright © 2019 Jacques Uwamun. All rights reserved.
//

import SwiftUI

struct CategoryItem : View {
    var category: Category
    
    var body: some View {

        VStack(alignment: .leading, spacing: 0.0) {
            Image(category.imageName)
                .resizable()
                .renderingMode(.original)
                .aspectRatio(contentMode: .fill)
                .frame(width: 250, height: 170)
                .cornerRadius(10)
                .shadow(radius: 10)
            Text(category.name)
                .color(.primary)
                .font(.headline)
                .padding(.top, 15)

        }
        .padding(.top, 0.0)
        
    }
}

#if DEBUG
struct CategoryItem_Previews : PreviewProvider {
    static var previews: some View {
        CategoryItem(category: categoriesData.first!)
    }
}
#endif
